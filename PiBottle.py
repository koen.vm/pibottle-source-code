import cv2
from pyzbar.pyzbar import decode
import json
import time

cap = cv2.VideoCapture(0)
cap.set(3, 640)
cap.set(4, 480)

x = 0.00
previousEan = "test"

print("StatiePi v1.0")
print("Waiting for barcode...")
while True:

    succes, img = cap.read()
    for barcode in decode(img):

        myData = int(barcode.data.decode('utf-8'))
        previousEan == myData

        with open('EanCodes.json', 'r') as file:
            data = json.load(file)
            for bottle in data['bottles']:
                if bottle.get('ean ce') == myData and myData != previousEan:
                    print("")
                    print("Barcode succesvol gelezen")
                    print("")
                    print("Type flesje: " + bottle.get('Artikelomschrijving'))
                    print("Statiegeld waarde: " + bottle.get('statiegeld waarde ce'))
                    x += 0.15
                    print("")
                    print("Totaal opgehaalde bedrag: €" + str(x))
                    previousEan = myData














